module.exports = function(grunt) {

  require('load-grunt-tasks')(grunt);
  require('time-grunt')(grunt);

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    // Minify
    // uglify: {
    //   my_target: {
    //     files: {
    //       'dest/output.min.js': ['src/input1.js', 'src/input2.js']
    //     }
    //   }
    // },

    // Spriting
    svgstore: {
      options: {
        prefix : 'icon-',
        formatting : {
          indent_size : 2
        }
      },
      default : {
        files: {
          'images/svg/sprite.svg': ['images/svg/source/*.svg']
        }
      }
    },

    // Merge files
    // concat: {
    //   options: {
    //     separator: ';',
    //   },
    //   dist: {
    //     src: ['js/home.js'],
    //     dest: 'js/script.js',
    //   },
    // },

    // SASS Compile
    sass: {
        options: {
            sourceMap: true
        },
        dist: {
            files: {
                'css/style.css': 'sass/style.scss'
            }
        }
    },

    // Watch Tasks
    watch: {
      css: {
        files: ['sass/*.scss', 'sass/**/*.scss'],
        tasks: ['sass'],
        options: {
          spawn: false
        }
      },
      // js: {
      //   files: ['**/*.js'],
      //   tasks: ['jshint', 'concat'],
      //   options: {
      //     spawn: false
      //   }
      // },
      svg: {
        files: ['images/svg/pre/*.svg'],
        tasks: ['svgstore'],
        options: {
          spawn: false
        }
      }
    }

  });

  // Load the plugin that provides the "uglify" task.
  // grunt.loadNpmTasks('grunt-contrib-uglify');

  // Default task(s).
  grunt.registerTask('sassy', ['sass']);
  grunt.registerTask('spritey', ['svgstore']);

};
