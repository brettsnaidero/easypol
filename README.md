# EASYPOL README #

PoliWHOtics, or Easypol, as some people prefer to call it, is a project first started at Hackagong 2015. It's a website which lists all the sitting members of the Australian parliament, easily sortable by party and house. Data is pulled in for each politician, allowing users to quickly see how that politician has voted in the past.

Using this thing for the time being: https://github.com/gplv2/CrossProxy

### What is this repository for? ###

This repo is for site development - thought it would be good to put this into a versioning system so we can work on things individually easily.

### How do I get set up? ###

Once you've cloned the repo to your computer, you'll want to run an 'npm install' in the folder to download all the node modules you'll need. From there, you can run Grunt tasks to compile SASS, combine SVG's, and more. 

The site relies on cross-domain AJAXing, working with They Vote For You's API to get politician data. We've implemented a PHP based proxy through which we can direct the requests, in order to work around cross-origin javascript limitations.

### Contribution guidelines ###

I'm not the greatest with Git, but if you create a branch (duplicating master into something such as 'brett-modulename') and then merge it back in when you're done or would like to test, we should be okay :)

### Who do I talk to? ###

brettsnaidero@hotmail.com