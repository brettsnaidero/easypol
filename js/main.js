// external js: isotope.pkgd.js
var filterFns = {
    // show if number is greater than 50
    numberGreaterThan50: function() {
        var number = $(this).find('.number').text();
        return parseInt( number, 10 ) > 50;
    },
    // show if name ends with -ium
    ium: function() {
        var name = $(this).find('.name').text();
        return name.match( /ium$/ );
    }
};

function getHashFilter() {
    // get filter=filterName
    var matches = location.hash.match( /filter=([^&]+)/i );
    var hashFilter = matches && matches[1];
    return hashFilter && decodeURIComponent( hashFilter );
}

// flatten object by concatting values
function concatValues( obj ) {
    var value = '';
    for ( var prop in obj ) {
        value += obj[ prop ];
    }
    return value;
}

// init Isotope
var $grid = $('.grid').isotope({
    itemSelector: '.grid-item',
    isInitLayout: false
});

$(document).ready( function() {

    // On page ready, load all politicians into page
    $.ajax({
        url: "../proxy/crossproxy.php",
        type: "GET",
        dataType: "text",
        success: function(data) {

            var returnedData = $.parseJSON(data);

            // Create all the politcians
            createPoliticians(returnedData);

            // Set up modal popups
            $('.open-popup-link').magnificPopup({
                type:'inline',
                callbacks: {
                    beforeOpen: function() {
                        this.st.mainClass = this.st.el.attr('data-effect');
                    },
                    close: function() {
                      // Clear the popup, and put a loading thing in it
                      $('#politician-popup .politician-content').html('<div class="loading">Loading</div>');
                      $('#politician-popup').removeClass('ready');
                      $('.politician-maps').html('');
                    }
                },
                midClick: true // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.
            });

            // If an ID has been passed with the url, then load that politician
            (function() {
              debugger;
              var myParam = location.search.split('id=')[1];
              $('#' + myParam).click();
            })();
        },
        error: function() {
            console.log("Failure!");
        }
    });

    $grid.imagesLoaded().progress( function() {
        $grid.isotope('layout');
    });

    // store filter for each group
    var filters = {};

    $('.filters').on( 'click', '.button', function() {
        var $this = $(this);
        // get group key
        var $buttonGroup = $this.parents('.button-group');
        var filterGroup = $buttonGroup.attr('data-filter-group');
        // set filter for group
        filters[ filterGroup ] = $this.attr('data-filter');
        // combine filters
        var filterValue = concatValues( filters );
        // set filter for Isotope
        $grid.isotope({ filter: filterValue });
    });

    // change is-checked class on buttons
    $('.button-group').each( function( i, buttonGroup ) {
        var $buttonGroup = $( buttonGroup );
        $buttonGroup.on( 'click', 'button', function() {
            $buttonGroup.find('.is-checked').removeClass('is-checked');
            $( this ).addClass('is-checked');
        });
    });

});

// Loop through data and create politicians
var createPoliticians = function(a) {
    var elems = [];
    for (i = 0; i < a.length; i++) {

        // ID Number
        var memId = a[i].id;

        // Name
        var name = a[i].latest_member.name.first + " " + a[i].latest_member.name.last;

        // Political Party
        var party = a[i].latest_member.party;

        // Electorate
        var electorate = a[i].latest_member.electorate;

        // Define party class names
        var classType = '';
        switch(party) {
            case "Liberal Party":
                classType = "lib";
            break;
            case "Australian Labor Party":
                classType = "lab";
            break;
            case "Australian Greens":
                classType = "greens";
            break;
            case "National Party":
                classType = "nats";
            break;
            case "Independent":
                classType = "indep";
            break;
            case "Liberal Democratic Party":
                classType = "libdem";
            break;
            case "Palmer United Party":
                classType = "palmer";
            break;
            default:
                classType ='none';
        }

        // House
        var house = a[i].latest_member.house;
        var houseClass = '';
        switch(house) {
            case "representatives":
                houseClass = "hor";
            break;
            case "senate":
                houseClass = "senate";
            break;
            default:
                houseClass ='none';
        }

        // Create div
        $elem = $('<div class="grid-item" />');
        $elem.addClass(classType);
        $elem.addClass(houseClass);
        $elem.append('<a class="open-popup-link" href="#politician-popup" id="' + memId + '" title=""><div class="politician-image"><div class="photo" style="background-image:url(images/photos/' + memId + '.jpg);"></div><div class="overlay"><div class="text">More Information &rarr;</div></div></div><div class="politician-name">' + name + '<div class="symbols"><div class="ch-icon ' + classType + '" title="' + party + '"></div></div></div></a>');

        elems.push( $elem[0] );
    }

    // append items to grid
    $grid.isotope( 'insert', elems );
}

var informationPolicy = {};

// Politician grab info
var indivPolitician = function(event) {

    event.preventDefault();

    // Get politician ID from element
    var memId= $(this).attr('id');

    // Check if cached, if not go get the data
    if (informationPolicy[memId] !== undefined) {
        console.log("it exists!");
        // Put the cached content back in
        // $('#politician-popup .politician-content').html(informationPolicy[memId]);
    } else {

    }

    var url = '../proxy/crossproxy-individuals.php';
    $.ajax({
        url,
        data: {
            userID: memId
        },
        type: 'GET',
        dataType: "text",
        success: function (data) {

            // Convert text to object for scripting
            var data = $.parseJSON(data);

            // Start organising the retrieved politician data
            var name = data.latest_member.name.first + ' ' + data.latest_member.name.last;
            var electorate = data.latest_member.electorate;

            // Positions
            var offices = '';
            if (data.offices.length > 0) {
                for (var i = 0; i < data.offices.length; ++i) {
                    offices += data.offices[i].position;
                    if (i !== data.offices.length - 1) {
                        offices += ' / ';
                    }
                }
            } else {
                offices = "No position currently held";
            }

            // Party
            var party = data.latest_member.party;

            // Party class
            var classType = '';
            switch(party) {
                case "Liberal Party":
                    classType = "lib";
                    break;
                case "Australian Labour Party":
                    classType = "lab";
                    break;
                case "Australian Greens":
                    classType = "greens";
                    break;
                 case "National Party":
                    classType = "nats";
                    break;
                case "Independent":
                    classType = "lib";
                    break;
                case "Liberal Democratic Party":
                    classType = "libdem";
                    break;
                case "Palmer United Party":
                    classType = "palmer";
                    break;
                case "Independent":
                    classType = "indep";
                    break;
                default:
                    classType ='none';
            }

            // House
            var house = data.latest_member.house;
            var houseClass = '';
            switch(house) {
                case "representatives":
                    houseClass = "hor";
                break;
                case "senate":
                    houseClass = "senate";
                break;
                default:
                    houseClass ='none';
            }

            // Three random votes
            var voteHistory = CreateHtmlFormat(data.policy_comparisons, memId);

            // Create the html
            var title = '<h2>' + name + '</h2>';
            var position ='<div class="position">' + offices + '</div>';
            var image = '<div class="photo" style="background-image: url(images/photos-resized/' + memId + '.jpg);"></div>';
            var partyType = '<div class="party ' + classType + '"> <!-- Party Class Here -->' + party + '</div>';

            informationPolicy[memId] = voteHistory;

            // House of Reps Politician
            if (house == 'representatives') {
              // Electorate Map
              getElectorate(memId, name, electorate);
              // House Map
              getRepHouse(house, party, memId);

            // Senate Politicians
            } else if (house == 'senate') {
              // State Map
              getState(memId, name, electorate);
              // House Map
              getSenHouse(house, party, memId);

            // If they aren't in a position anymore
            } else {
              var maps = '<div class="politician-house"><h3>No Seat</h3></div>';
              $('#politician-popup .politician-maps').append('div class="house-map"></div>');
              $('.politician-maps .house-map').html(maps);
            }

            // Loading the data in!
            // Politician Information
            $('#politician-popup .politician-info .title').html(title);
            $('#politician-popup .politician-info .position').html(position);
            $('#politician-popup .politician-info .image').html(image);
            $('#politician-popup .politician-info .party-spot').html(partyType);
            // Politician Vote Content
            $('#politician-popup .politician-content').html(informationPolicy[memId]);

            // Add 'ready' class to popup, to initiate animations
            $('#politician-popup').addClass('ready').delay(1000);
        },
        error: function () {
            console.log("it didn't work");
        }
    });
}

function getElectorate(memId, name, electorate){
    $.ajax({
        url: 'map/electorate.html',
        type: 'GET',
        dataType: "html",
        success: function(data){
            // Politician Maps
            var maps = '<div class="politician-electorate"><h3>Australian Federal Electorates</h3>' +
            data +
            '<div class="member">' + name + ' is the member for <span>' + electorate + '</span></div></div>';

            $('#politician-popup .politician-maps').append('<div class="electorate-map"></div>');
            $('.politician-maps .electorate-map').append(maps);

            // Highlight Electorate
            var $thing = $(".electorate-map svg").find("[data-number='" + memId + "']");
            $thing.attr("class", "electorate selected");
        }
    });
}

function getState(memId, name, electorate){
    $.ajax({
        url: 'map/states.html',
        type: 'GET',
        dataType: "html",
        success: function(data){
            // Politician Maps
            var maps = '<div class="politician-electorate"><h3>Senate State</h3>' +
            data +
            '<div class="member">' + name + ' is the member for <span>' + electorate + '</span></div></div>';

            $('#politician-popup .politician-maps').append('<div class="electorate-map"></div>');
            $('.politician-maps .electorate-map').append(maps);

            // Highlight Electorate
            var $thing = $(".electorate-map svg").find("#" + electorate);
            $thing.attr("class", "electorate selected");
        }
    });
}

function getRepHouse(house, party, memId) {
    $.ajax({
        url: 'map/houseofreps.html',
        type: 'GET',
        dataType: 'html',
        success: function(data) {
          var maps = '<div class="politician-house"><h3>House of Representatives Seat</h3>' +
          data +
          '</div>';

          $('#politician-popup .politician-maps').append('<div class="house-map"></div>');
          $('.politician-maps .house-map').html(maps);

          // Highlight Seat
          var $thing = $(".house-map svg").find("[data-sitting='" + memId + "']");
          $thing.attr("class", "bit2 activeSeat selected " + party );
        }
    });
}

function getSenHouse(house, party, memId) {
    $.ajax({
        url: 'map/senate.html',
        type: 'GET',
        dataType: 'html',
        success: function(data) {
          var maps = '<div class="politician-house"><h3>Senate Seat</h3>' +
          data +
          '</div>';

          $('#politician-popup .politician-maps').append('<div class="house-map"></div>');
          $('.politician-maps .house-map').html(maps);

          // Highlight Seat
          var $thing = $(".house-map svg").find("[data-sitting='" + memId + "']");
          $thing.attr("class", "bit2 activeSeat selected");
        }
    });
}

// Listen for clicks on grid items to load politician info
// Note: this uses a 'live' listener because the items are loaded with js after the page load
$(document.body).on( "click", '.grid-item a', indivPolitician);

// Create HTML for vote history
var CreateHtmlFormat = function(data, memId) {
    var value = 4;
    var content = '';
    if (data.length < 4) {
        value = data.length;
    }
    for (var i = 0; i < value-1; i++) {
        //<!-- Loop 3 of these -->
        var numberCrazy = Math.floor((Math.random() * 30) + 1);
        var id = 'issue'+i+memId;
        var percent = Math.abs(data[numberCrazy].agreement-50);
        var circle = (percent * 3.6);
        var yaynay;
        var believe;

        if(data[i].agreement > 50) {
            yaynay = "Voted <span>in favour</span> of ";
            believe = "believe";
        } else if (data[i].agreement < 50) {
            yaynay = "Voted <span>against</span> ";
            believe = "do not believe";
        } else if (data[i].agreement = 50) {
            yaynay= "Has never voted on ";
        } else {
            console.log("Might need to rethink these");
        }
        content =
          content +
          '<div class="bitlet clearfix">' +
          '<div class="swing-title">' +
          yaynay +
          data[i].policy.name +
          '.</div><div class="swing-bar">'+

          '<div class="progress-pie-chart" data-percent="' + percent + '">' +
          '<div class="ppc-progress">' +
          '<div class="ppc-progress-fill" id="' + id +
          '"></div>' +
          '</div>' +
          '<div class="ppc-percents">' +
          '<div class="pcc-percents-wrapper">' +
          '<span>' + percent + '%</span>' +
          '</div>' +
          '</div>'+
          '</div>' +

          '<div class="swing right" id="' +
          id +
          '"></div><style>#politician-popup.ready #' +
          id + ' { transform: rotate(' +
          circle +
          'deg); /* For or against value calculated */ } </style> </div> <div class="swing-description">' +
          'They ' + believe +
          ' ' +
          data[i].policy.description +
          '.</div></div>';
    }

    return content;
}
