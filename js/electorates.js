function initMap() {

  var styles = [
      {
          "featureType": "administrative",
          "elementType": "labels.text.fill",
          "stylers": [
              {
                  "color": "#444444"
              }
          ]
      },
      {
          "featureType": "landscape",
          "elementType": "all",
          "stylers": [
              {
                  "color": "#f2efea"
              },
              {
                  "visibility": "on"
              }
          ]
      },
      {
          "featureType": "landscape",
          "elementType": "labels.text.fill",
          "stylers": [
              {
                  "color": "#3c3c3b"
              }
          ]
      },
      {
          "featureType": "landscape.natural.terrain",
          "elementType": "labels",
          "stylers": [
              {
                  "visibility": "off"
              }
          ]
      },
      {
          "featureType": "poi",
          "elementType": "all",
          "stylers": [
              {
                  "visibility": "off"
              }
          ]
      },
      {
          "featureType": "road",
          "elementType": "all",
          "stylers": [
              {
                  "saturation": -100
              },
              {
                  "lightness": 45
              }
          ]
      },
      {
          "featureType": "road.highway",
          "elementType": "all",
          "stylers": [
              {
                  "visibility": "simplified"
              }
          ]
      },
      {
          "featureType": "road.arterial",
          "elementType": "labels.icon",
          "stylers": [
              {
                  "visibility": "off"
              }
          ]
      },
      {
          "featureType": "transit",
          "elementType": "all",
          "stylers": [
              {
                  "visibility": "off"
              }
          ]
      },
      {
          "featureType": "water",
          "elementType": "all",
          "stylers": [
              {
                  "color": "#BFCCD2"
              },
              {
                  "visibility": "on"
              }
          ]
      }
  ]

  // Create a map object and specify the DOM element for display.
  var map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: -24.994167, lng: 134.866944},
    scrollwheel: false,
    zoom: 4,
    styles: styles
    // disableDefaultUI: true
  });

  map.data.loadGeoJson('map/electorates/electorates-compressed2-5.json');

  map.data.setStyle({
    fillColor: '#008cc9',
    strokeWeight: 1
  });

  // Set mouseover event for each feature.
  map.data.addListener('mouseover', function(event) {

  });

  // global infowindow
  var infowindow = new google.maps.InfoWindow();

  // When the user clicks, open an infowindow
  map.data.addListener('click', function(event) {
      var electorate = event.feature.getProperty("SORTNAME");
      var myHTML = '<div class="electorate-title">Electorate of <span>' + electorate + '</span></div>';

      // Ajax call to get politician for this electorate
      var url = '../proxy/crossproxy-individuals.php';
      $.ajax({
          url,
          data: {
              userID: 10001
          },
          type: 'GET',
          dataType: "text",
          success: function (data) {
            // Convert text to object for scripting
            var data = $.parseJSON(data);
            var name = data.latest_member.name.first + ' ' + data.latest_member.name.last;
            var party = data.latest_member.party;
            var image = '<div class="photo" style="height: 60px; width: 60px; background-size: cover; background-image: url(images/photos-resized/' + '10001' + '.jpg);"></div>';

            myHTML = myHTML + '<div class="name">Current minister: ' + name + '</div>' + '<div>' + party + '</div>' + image + '<a href="politicians.html?id=' + '10001' + '" title="">More about ' + name + ' &raquo;</a>';

            infowindow.setContent("<div style='width:150px; text-align: center;'>"+myHTML+"</div>");

          },
          error: function () {
              console.log("it didn't work");
          }
      });

      // Create an cnhor point where the mouse click is
      var anchor = new google.maps.MVCObject();
      anchor.setValues({ //position of the point
          position: event.latLng,
          anchorPoint: new google.maps.Point(0, 0)
      });

      // Open popup at this anchor point
      infowindow.open(map, anchor);
  });

  // On page ready, load politicians and iterate through them, assinging their parties to the electorates
  $.ajax({
      url: "../proxy/crossproxy.php",
      type: "GET",
      dataType: "text",
      success: function(data) {
          var returnedData = $.parseJSON(data);
          // Electorate
          var electoratesSitting = {};
          for (i = 0; i < returnedData.length; i++) {
            var electorate = returnedData[i].latest_member.electorate;
            var party = returnedData[i].latest_member.party;
            var classType = '';
            switch(party) {
                case "Liberal Party":
                    classType = "lib";
                break;
                case "Australian Labor Party":
                    classType = "lab";
                break;
                case "Australian Greens":
                    classType = "greens";
                break;
                case "National Party":
                    classType = "nats";
                break;
                case "Independent":
                    classType = "indep";
                break;
                case "Liberal Democratic Party":
                    classType = "libdem";
                break;
                case "Palmer United Party":
                    classType = "palmer";
                break;
                default:
                    classType ='none';
            }
            electoratesSitting[electorate] = classType;
          }

          // Change colour based on things
          map.data.setStyle(function(feature) {
              var name = feature.getProperty('ELECT_DIV');
              var elecName = electoratesSitting[name];
              console.log(elecName);
              var color = '';
              switch( elecName ) {
                  case "lib":
                      color = "blue";
                  break;
                  case "lab":
                      color = "red";
                  break;
                  case "greens":
                      color = "green";
                  break;
                  case "nats":
                      color = "teal";
                  break;
                  case "indep":
                      color = "purple";
                  break;
                  case "libdem":
                      color = "black";
                  break;
                  case "palmer":
                      color = "yellow";
                  break;
                  default:
                    // Nadaq
              }
              // var color = ascii > 91 ? 'red' : 'blue';
              // var myObject = electoratesSitting.filter(function(element) {
              //     return element === name;
              //     console.log('Huh?');
              // }


              return {
                fillColor: color,
                strokeWeight: 1
              };
          });
      },
      error: function() {
          console.log("Failure!");
      }
  });

}
