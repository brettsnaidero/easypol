$.get("images/svg/sprite.svg", function(data) {
  var div = document.createElement("div");
  div.innerHTML = new XMLSerializer().serializeToString(data.documentElement);
	div.style.height = "0px";
	div.style.width = "0px";
	div.style.position = "absolute";
	div.style.visibility = "hidden";
  document.body.insertBefore(div, document.body.childNodes[0]);
});
