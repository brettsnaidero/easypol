<!doctype html>

<html>

<head>

    <meta charset="utf-8">

    <!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <!-- Viewport Set -->
    <meta http-equiv="cleartype" content="on">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Testing PHP Cross Domain Proxy</title>

    <link rel="shortcut icon" href="images/favicon.ico">

</head>

<body style="background-color: #FFFFFF; padding: 20px; font-family: Arial, sans-serif;">
    <div id="click" style="padding: 16px 0px; width: 300px; text-align: center; display: inline-block; background-color: #73CC5D; color: #FFFFFF; font-weight: bold; cursor: pointer;">
        Click Here
    </div>
    <div id="return" style="height: 300px; width: 280px; padding: 10px; border: 1px solid #222222; margin: 10px 0px 0px 0px; color: #000000; overflow: scroll;">

    </div>

    <!-- jQuery -->
    <script src="../js/thirdparty/jquery/jquery.min.js"></script>
    <script>
        $('#click').click(
            function() {
                $('#return').load(
                    "crossproxy.php"
                );
            }
        );
    </script>
</body>

</html>
